﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System.IO;

public class ButtonController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    public int tipo;
    public GameObject goPanelMessage;
    public InfoHome infoHome;
    public GameObject goRoot;
    public GameObject goPanelMenu;
    public GameObject goPanelOptions;

    GameObject goMessage;
    Canvas cPanel;
    Image imgMessage;
    TextMeshProUGUI tmTitle;
    TextMeshProUGUI tmMessage;
    Button buttonClose;
    Button buttonClicked;
    ButtonMenu buttonMenuClicked;
    GameObject goRowOptions;
    static List<GameObject> listOptions;
    string downloadUrl;

    public int timeremain = 2; // tiempo restante
    Button _button;
    public OVRCameraRig camera;
    public Button ButtonMenu;
    static bool isBienvenida;
    static Button buttonBienvenida;    

    void Start() {

        if (tipo == 2){
            buttonClicked.onClick.AddListener(ShowMenu);
        }else if (tipo == 3){
            buttonClicked.onClick.AddListener(OpenOptions);
        }else {
            buttonClicked = this.GetComponentInChildren<Button>();
            buttonClicked.onClick.AddListener(FillMessage);
        }
    }

    void ShowMenu(){

        goPanelMenu.gameObject.SetActive(true);
        goPanelMenu.transform.localScale = Vector3.zero;
        Sequence seq = DOTween.Sequence();
        seq.Append(goPanelMenu.transform.DOScale(1, 0.5f));

        /*listButtons = goPanelMenu.GetComponentsInpanelMenutton> ();
		foreach (Button item in listButtons) {
			item.transform.localScale = Vector3.zero;
		}
		Sequence seq = DOTween.Sequence();
		seq.Append (goPanelMenu.transform.DOScapanelMenu.5f));
		seq.Join (goPanelMenu.transform.DOScapanelMenu));
		StartCoroutine (showButtons(seq));*/
    }

    void CloseMessage(){
        Destroy(goMessage);
        //goMessage.SetActive (false);
    }

    public void OnPointerEnter(PointerEventData eventData){
        //do your stuff when highlighted
        NotificationCenter.DefaultCenter().PostNotification(this, "EnBoton");
        //print("en boton");
        InvokeRepeating("countDown", 1, 1);//llama al cursor
    }

    public void OnPointerExit(PointerEventData eventData){
        //do your stuff when highlighted
        NotificationCenter.DefaultCenter().PostNotification(this, "EnNada");
        print("Cancela");
        CancelInvoke("countDown");
        timeremain = 2;
    }

    void countDown() {
        //print(timeremain);
        if (timeremain <= 0) {
            NotificationCenter.DefaultCenter().PostNotification(this, "EnNada");
            buttonClicked.onClick.Invoke();
            //reset time
            CancelInvoke("countDown");
            timeremain = 2;
            //print("reset time");
        }
        timeremain--;
    }
    
    void OpenOptions()
    {
        string buttonName = buttonMenuClicked.name;

        Debug.Log(buttonMenuClicked.listOptions.Count);
        if (buttonMenuClicked.listOptions.Count > 0)
        {
            showgoPanelOptions(buttonMenuClicked);
        }
        else
        {
            //StartCoroutine(DownloadPlan());
        }

    }

    IEnumerator DownloadPlan()
    {

        WWW _www = new WWW(downloadUrl);
        yield return new WaitUntil(() => _www.isDone);

        string fileName = Application.streamingAssetsPath + "planos.zip";

        if (_www.bytes.Length > 0)
        {
            Debug.Log(fileName);
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
            File.WriteAllBytes(fileName, _www.bytes);
        }

    }

    void showgoPanelOptions(ButtonMenu objButtonName){

        CleanOptions();
        foreach (ButtonOptions item in objButtonName.listOptions)
        {
            GameObject goRow = Instantiate(goRowOptions, goPanelOptions.transform);
            listOptions.Add(goRow);
            goRow.GetComponentInChildren<TextMeshProUGUI>().text = item.name;
            Toggle buttonOption = goRow.GetComponentInChildren<Toggle>();
            buttonOption.onValueChanged.AddListener((bool value) => InactiveElement(item.GoStructure));
        }
        goPanelOptions.gameObject.SetActive(true);
        goPanelOptions.transform.localScale = Vector3.zero;
        Sequence seq = DOTween.Sequence();
        seq.Append(goPanelOptions.transform.DOScale(1, 0.5f));
    }

    void CleanOptions() { 
        int index = listOptions.Count - 1;

        while (index >= 0)
        {
            Debug.Log(index);
            Destroy(listOptions[index]);
            index--;
        }
    }

    void FillMessage() {

        Debug.Log("FILL MESSAGE");
        Transform tPanelTexto;
        Transform tPanel;

        if (goRoot.transform.childCount > 0) {
            Destroy(goRoot.transform.GetChild(0).gameObject);
        }

        if (tipo == 1) {
            Vector3 newPosition = new Vector3(this.transform.position.x, this.transform.position.y + 2f, this.transform.position.z);
            camera.transform.position = newPosition;
            if (this.name == "ButtonBienvenida") {
                isBienvenida = true;
                buttonBienvenida = this.GetComponentInChildren<Button>();
            } else {
                isBienvenida = false;
            }
            tPanel = this.transform.GetChild(1);
        } else {
            if (isBienvenida) {
                tPanel = buttonBienvenida.transform.GetChild(1);
            } else {
                tPanel = this.transform.GetChild(1);
            }
        }

        RectTransform rtPosition = tPanel.GetComponentInChildren<RectTransform>();
        Debug.Log(rtPosition.position);
        goMessage = Instantiate(goPanelMessage, rtPosition.position, rtPosition.rotation);
        goMessage.transform.SetParent(goRoot.transform);
        goMessage.SetActive(false);

        cPanel = goMessage.GetComponentInChildren<Canvas>();
        buttonClose = cPanel.transform.GetChild(0).GetComponent<Button>();
        buttonClose.onClick.AddListener(CloseMessage);
        imgMessage = cPanel.transform.GetChild(1).transform.GetChild(0).GetComponentInChildren<Image>();
        tPanelTexto = cPanel.transform.GetChild(1).transform.GetChild(1);
        tmTitle = tPanelTexto.GetChild(0).GetComponentInChildren<TextMeshProUGUI>();
        tmMessage = tPanelTexto.GetChild(2).GetComponentInChildren<TextMeshProUGUI>();

        foreach (var item in infoHome.listMessageArch) {

            Debug.Log(item.name);

            if (this.name.IndexOf(item.name) > -1) {

                if (item.tipoMenssage == 1) {
                    imgMessage.gameObject.SetActive(false);
                } else {
                    imgMessage.sprite = item.messageImage;
                }

                tmTitle.text = item.title;
                tmMessage.text = item.description;
                goMessage.SetActive(true);
                break;
            }
        }
    }

    public void InactiveElement(List<GameObject> goStructure){

        foreach (var item in goStructure){
            GameObject goEstruct = GameObject.Find("Home/casa/" + item.name);
            Debug.Log(goEstruct.name);
            bool isActive = goEstruct.activeSelf ? false : true;
            goEstruct.SetActive(isActive);
        }
    }

    public Button ButtonClicked{
        set { buttonClicked = value; }
        get { return buttonClicked; }
    }
    public ButtonMenu ButtonMenuClicked {
        set { buttonMenuClicked = value; }
        get { return buttonMenuClicked; }
    }

    public GameObject GoRowOptions {
        set { goRowOptions = value; }
        get { return goRowOptions; }
    }
}
