﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu()]
public class Menu : ScriptableObject {
	public List<ButtonMenu> listButtons;

    public static void SetChecked(string menuItemName, bool setPrefsForUtilities)
    {
        //throw new NotImplementedException();
    }
}
[System.Serializable]
public class ButtonMenu {

	public string name;
	public Image image;
	public List<ButtonOptions> listOptions;
}
[System.Serializable]
public class ButtonOptions{	
	public string name;
	public Button ButtonActive;
	public List<GameObject> GoStructure;
}
