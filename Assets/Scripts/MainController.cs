﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System.IO;

public class MainController : MonoBehaviour{

    public Button buttonMenu;
    public GameObject goPanelMenu;
    public GameObject goPanelOptions;
    public GameObject goPanelMessageArch;
    public GameObject goPanelMessageStruct;
    public GameObject goRowOptions;
    public GameObject goButtonMenu;
    public GameObject CameraRig1;
    public GameObject CameraRig2;
    public GameObject goRoot;
    public Menu Menu;
    public InfoHome InfoHome;
    public Canvas canvas;
    public string downloadUrl;
    
    
    // Use this for initialization
    void Start() {
        
        Initiatemenu();
        Camera camera;
        GameObject goBienvenida = GameObject.Find("ButtonBienvenida");
        GameObject goOVRGazePointer = GameObject.Find("OVRGazePointer");
        GameObject goEventSystem = GameObject.Find("EventSystem");

#if UNITY_ANDROID || UNITY_IOS        
    camera = CameraRig2.transform.Find("Camera (head)").GetComponentInChildren<Camera>();
#else
    camera = CameraRig1.transform.GetChild(0).transform.Find("CenterEyeAnchor").GetComponentInChildren<Camera>();
#endif

    canvas.worldCamera = camera;
    goOVRGazePointer.GetComponentInChildren<OVRGazePointer>().rayTransform = camera.transform;
    goEventSystem.GetComponentInChildren<OVRInputModule>().rayTransform = camera.transform;

#if UNITY_ANDROID || UNITY_IOS
    MoveCamera(goBienvenida, CameraRig2);
#else
    MoveCamera(goBienvenida, CameraRig1);
#endif

    }

    void MoveCamera(GameObject goBienvenida, GameObject cameraRig){
        RectTransform rtBienvenidaPos = goBienvenida.GetComponentInChildren<RectTransform>();
        Vector3 newPosition = new Vector3(rtBienvenidaPos.position.x, rtBienvenidaPos.position.y + 2f, rtBienvenidaPos.position.z);
        cameraRig.transform.DOMove(newPosition,6f).SetEase(Ease.InOutSine).SetDelay(0.5f).OnComplete(()=> {
            goBienvenida.GetComponentInChildren<Button>().onClick.Invoke();
        });
    }

	void Initiatemenu(){

        buttonMenu.gameObject.AddComponent<ButtonController>();
        ButtonController objButtonControler =  buttonMenu.gameObject.GetComponentInChildren<ButtonController>();
        objButtonControler.goPanelMenu = goPanelMenu;
        objButtonControler.goPanelOptions = goPanelOptions;
        objButtonControler.tipo = 2;
        objButtonControler.ButtonClicked = buttonMenu;
        
		float angle = 360f/Menu.listButtons.Count;
		var listmenuButtons = Menu.listButtons;        

        foreach (ButtonMenu item in listmenuButtons) {
            Debug.Log("item.name " + item.name);
            Button buttonMenu = goButtonMenu.transform.GetChild(0).GetComponentInChildren<Button>();
            RectTransform rtButtonMenu = buttonMenu.GetComponentInChildren<RectTransform>();
            GameObject goButton = Instantiate(buttonMenu.gameObject, rtButtonMenu.position, new Quaternion(90, 0, 0, 0), goPanelMenu.transform);            
            Text textButtonName = goButton.GetComponentInChildren<Text> ();
			Button buttonSubMenu = goButton.GetComponentInChildren<Button> ();
			textButtonName.text = item.name;
            buttonSubMenu.gameObject.AddComponent<ButtonController>();
            ButtonController objButtonControlerOptions = buttonSubMenu.gameObject.GetComponentInChildren<ButtonController>();
            objButtonControlerOptions.goPanelMenu = goPanelMenu;
            objButtonControlerOptions.goPanelOptions = goPanelOptions;
            objButtonControlerOptions.tipo = 3;
            objButtonControlerOptions.ButtonClicked = buttonSubMenu;
            objButtonControlerOptions.ButtonMenuClicked = item;
            objButtonControlerOptions.goRoot = goRoot;
		}
	}

	void ShowMenu(){
            
		goPanelMenu.gameObject.SetActive (true);
		goPanelMenu.transform.localScale = Vector3.zero;
		Sequence seq = DOTween.Sequence();
		seq.Append (goPanelMenu.transform.DOScale (1, 0.5f));

		/*listButtons = goPanelMenu.GetComponentsInpanelMenutton> ();
		foreach (Button item in listButtons) {
			item.transform.localScale = Vector3.zero;
		}
		Sequence seq = DOTween.Sequence();
		seq.Append (goPanelMenu.transform.DOScapanelMenu.5f));
		seq.Join (goPanelMenu.transform.DOScapanelMenu));
		StartCoroutine (showButtons(seq));*/
	}
    
	/*IEnumerator showButtons (Sequence seq){

		int cantButtons = listButtons.Count;
		int index = 0;

		foreach (ButtonController item in listButtons) {
			bool complete = false;

			seq.Join (item.transform.DOScale (1f, 0.01f).SetEase(Ease.InSine).OnComplete(()=>{				
					complete = true;					
			}));

			yield return new WaitWhile (() => complete==false);
			yield return new WaitForSeconds (0.03f);

			index++;
		}

		yield return new WaitWhile (() => index < cantButtons);

	}*/

	

}
