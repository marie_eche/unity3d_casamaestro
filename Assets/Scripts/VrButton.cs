﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VrButton : MonoBehaviour {

    	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnButtonEnter() {
        Debug.Log("Button Enter!");
    }
    public void OnButtonExit() {
        Debug.Log("Button Exit!");
    }
    public void OnClick() {
        Debug.Log("Clicked!");
    }

}
