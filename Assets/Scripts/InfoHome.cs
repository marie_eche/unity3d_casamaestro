﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class InfoHome: ScriptableObject{
	public List<Message> listMessageArch;
}
[System.Serializable]
public class Message {

	public string name;
	public string title;
	public string description;
	public Sprite messageImage;
	public int tipoMenssage;
	public string url;
}
