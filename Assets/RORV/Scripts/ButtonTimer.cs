﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;



public class ButtonTimer : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public int timeremain = 2; // tiempo restante
    Button _button;
    public OVRCameraRig camera;
    public Button ButtonMenu;



    
    // Use this for initialization
    void Start () {

        _button = GetComponent<Button>();
   
    }

    void Update()
    { 
 
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        //do your stuff when highlighted
        NotificationCenter.DefaultCenter().PostNotification(this, "EnBoton");
        //print("en boton");
        InvokeRepeating("countDown", 1, 1);//llama al cursor
        

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //do your stuff when highlighted
        NotificationCenter.DefaultCenter().PostNotification(this, "EnNada");
        print("Cancela");
        CancelInvoke("countDown");
        timeremain = 2;
    }

    void countDown()
    {
       
        //print(timeremain);
        
        
        if (timeremain <= 0)
        {

            NotificationCenter.DefaultCenter().PostNotification(this, "EnNada");
            _button.onClick.Invoke();
            //reset time
            CancelInvoke("countDown");
            timeremain = 2;
            //print("reset time");
            
        }

        timeremain--;
    }

    public void Clicked() {

        Vector3 newPosition = new Vector3 (this.transform.position.x,this.transform.position.y + 1f, this.transform.position.z);
        camera.transform.position = newPosition;
        

        print("clicked");
    }

    /*
    protected virtual Vector3 GetNewPosition(Vector3 tipPosition, Transform target, bool returnOriginalPosition)
    {
        if (returnOriginalPosition)
        {
            return tipPosition;
        }

        float newX = 0f;
        float newY = 0f;
        float newZ = 0f;

       
       newX = (headsetPositionCompensation ? (tipPosition.x - (headset.position.x - playArea.position.x)) : tipPosition.x);
       newY = playArea.position.y;
       newZ = (headsetPositionCompensation ? (tipPosition.z - (headset.position.z - playArea.position.z)) : tipPosition.z);
      

        return new Vector3(newX, newY, newZ);
    }*/




}
